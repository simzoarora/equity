import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
declare var $: any;
import { NgbDateParserFormatter, NgbDate, NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { AuthService } from 'src/app/service/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SearchdataService } from 'src/app/service/searchdata.service';
import { NgbDateFRParserFormatter } from "../../ngb-date-fr-parser-formatter";
import { ToastrService } from 'ngx-toastr';
require("inputmask/dist/inputmask/inputmask.numeric.extensions");
var Inputmask = require("inputmask/dist/inputmask/inputmask.date.extensions");
class Person {
  id: number;
  firstName: string;
  lastName: string;
}

class DataTablesResponse {
  data: any[];
  draw: number;
  recordsFiltered: number;
  recordsTotal: number;
}


class Action {
  id: number;
  status: number;
}

class SearchCount {
  count: number;
  perPropertyRate: number;
  amountReq() {
    return this.count * this.perPropertyRate;
  }
}


class Selection {
  state: any;
  countySelect: string;
  county: any[];
  citySelect: string;
  zipSelect: string;
  city: any[];
  zipcode: any;
  land: any[];
  exemption: any[];
  occupancy: any[];
  saleSelect: string;
  salesFrom: any;
  salesTo: any;
  mortgageAmountSelect: string;
  mortgageAmountFrom: string;
  mortgageAmountTo: string;
  mortgageRecordingDate: string;
  mortgageRecordingFrom: any;
  mortgageRecordingTo: any;
  mortgageType: any[];
  mortgageInterestStatus: string;
  mortgageInterestFrom: string;
  mortgageInterestTo: string;
  maxOpenLien: any[];
  equityStatus: string;
  equityFrom: string;
  equityTo: string;
  listingStatus: any[];
  listingPriceStatus: string;
  listingPriceFrom: string;
  listingPriceTo: string;
  forclosureStatus: any;
  forclosureDateStatus: string;
  forclosureDateFrom: any;
  forclosureDateTo: any;
  forclosureAmountStatus: string;
  forclosureAmountFrom: string;
  forclosureAmountTo: string;
  savedTitle: string;
  purchaseGroupName: string;
}
class SelectDefault {
  county: any[];
  city: any[];
  land: any[];
  // landRValue:any[];
  // landCValue:any[];
  exemption: any[];
  occupancy: any[];
  mortgageType: any[];
  maxOpenLien: any[];
  listingStatus: any[];
}
@Component({
  selector: 'app-advance-customer',
  templateUrl: './advance-customer.component.html',
  styleUrls: ['./advance-customer.component.scss'],
  providers: [{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter }]
})
export class AdvanceCustomerComponent implements OnInit {
  load:number=1;
  selectedCounty = [];
  selectedCity = [];
  selectedLand = [];
  selectedExemption = [];
  selectedOccupancy = [];
  selectedMortgageType = [];
  selectedMaxOpenLien = [];
  selectedListingStatus = [];
  countyLoading: boolean = false;
  cityLoading: boolean = false;
  advanceSearchFilledForm = JSON.parse(localStorage.getItem('advanceSearchForm'));
  //advanceSearchFilledForm:any = null;
  d = new Date();

  minDate = { year: 1900, month: 1, day: 1 };
  maxDate = {};
  zipError: boolean = false;
  countError: boolean = false;
  existingMember: boolean;
  modalTitle: string;
  modalContent: string;
  reqAmountInWalletToPur: boolean;
  confirmBoxContent: string;
  confirmBoxTblAmt: any = [50, 100, 200, 500, 1000, 2000];
  confirmBoxReqAmt: number;

  saleDateError: boolean = false
  openAmountError: boolean = false
  openDateError: boolean = false
  openInterestError: boolean = false
  equityError: boolean = false
  listingError: boolean = false
  forDateError: boolean = false
  forAmountError: boolean = false

  myForm: any;
  selection = new Selection();
  selectDefault = new SelectDefault();
  SearchCount = new SearchCount();

  data: any = [];
  kicks: any[];
  state: any[];
  county: any[];
  city: any[];
  title = 'angulardatatables';
  submit: boolean = false;
  action = new Action()
  model: NgbDateStruct;
  date: { year: number, month: number };

  submitSave: boolean = false;
  closeResult:string
  searchForm: FormGroup;
  saveSearchForm: FormGroup;
  checkForm: FormGroup;
  savedsearchFlag: boolean = false;
  months: any = [];
  years: any = [];
  submitWallet: boolean = false; 

  // result:any=[]
  currentBalance: number;
  depositeForm: FormGroup;
  dtTriggerWallet: Subject<any> = new Subject();
  @ViewChild('content2',{static:false}) private content2;
  constructor(private authService: AuthService, private router: Router, private calendar: NgbCalendar, private fb: FormBuilder, private modalService: NgbModal, private adSrData: SearchdataService, private elRef: ElementRef, private toastr: ToastrService) {
    
    const yesterday= new Date()
    yesterday.setDate(yesterday.getDate() - 1);  
    this.maxDate= {year: this.d.getFullYear(), month: (this.d.getMonth()), day: yesterday.getDate() }
    
    for (let index = 1; index <= 12; index++) {
      this.months.push(index);
    }
    let year = new Date().getFullYear()
    for (let index = year; index <= (year + 25); index++) {
      this.years.push(index);
    }
    this.searchForm = this.fb.group({
      state: [null],
      countySelect: ['is'],
      county: [''],
      citySelect: ['is'],
      zipSelect: ['is'],
      city: [''],
      zipcode: ['', [Validators.pattern('^[0-9]{5}(?:-[0-9]{4})?$')]],
      land: [''],
      // landRValue:[''],
      // landCValue:[''],
      exemption: [''],
      occupancy: [''],
      saleSelect: ['is between'],
      salesFrom: [''],
      salesTo: [''],
      mortgageAmountSelect: ['is between'],
      mortgageAmountFrom: [''],
      mortgageAmountTo: [''],
      mortgageRecordingDate: ['is between'],
      mortgageRecordingFrom: [''],
      mortgageRecordingTo: [''],
      mortgageType: [''],
      mortgageInterestStatus: ['is between'],
      mortgageInterestFrom: [''],
      mortgageInterestTo: [''],
      maxOpenLien: [''],
      equityStatus: ['is between'],
      equityFrom: [''],
      equityTo: [''],
      listingStatus: [''],
      listingPriceStatus: ['is between'],
      listingPriceFrom: [''],
      listingPriceTo: [''],
      forclosureStatus: [null],
      forclosureDateStatus: ['is between'],
      forclosureDateFrom: [{ value: '', disabled: true }],
      forclosureDateTo: [{ value: '', disabled: true }],
      forclosureAmountStatus: ['is between'],
      forclosureAmountFrom: [{ value: '', disabled: true }],
      forclosureAmountTo: [{ value: '', disabled: true }],
      savedTitle: [''],
      purchaseGroupName: ['']
    })

    this.saveSearchForm = this.fb.group({
      savedTitle: ['', [Validators.maxLength(25)]],
      purchaseGroupName: ['', [Validators.pattern('^([-a-zA-Z0-9_ ])+$'), Validators.maxLength(25)]]
    })

    this.checkForm = this.fb.group({
      saved: ['', [Validators.required]]
    })

    this.selection.countySelect = 'is'
    this.selection.citySelect = 'is'
    this.selection.zipSelect = 'is'
    this.selection.saleSelect = 'is between'
    this.selection.mortgageAmountSelect = 'is between'
    this.selection.mortgageRecordingDate = 'is between'
    this.selection.mortgageInterestStatus = 'is between'
    this.selection.equityStatus = 'is between'
    this.selection.listingPriceStatus = 'is between'
    this.selection.forclosureDateStatus = 'is between'
    this.selection.forclosureAmountStatus = 'is between'
    this.SearchCount.count = 0;
    this.authService.getState()
      .subscribe(data => {
        this.state = data.data
      }, error => {

      })
  }

  open(content) {
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      // alert('Closed with:' + result)// this.closeResult = `Closed with: ${result}`;
      if (result == 'Submit') {
        this.submitSave = true;
        if (this.saveSearchForm.status == 'VALID') {
          this.savedsearchFlag = true;
        }
        this.submitSave = false;
        this.saveSearchForm.reset();
      }
    }, (reason) => {
      this.submitSave = false;
      this.saveSearchForm.reset();
    });
    /* if(content._def.references.hasOwnProperty('confirmModal')){
       this.getResult();
     }*/
  }

  changeState(arg) {
    this.countyLoading = true;
    this.cityLoading = true;
    $('[formControlName="county"]').addClass('selectLoading');
    $('[formControlName="city"]').addClass('selectLoading');
    var indexSel = arg.target.selectedIndex;
    var label = arg.srcElement[indexSel].label;
    var value = arg.srcElement[indexSel].value;
    this.selection.state = { val: value, text: label };
    this.selectedCounty = [];
    this.selection.county = [];
    this.setCounty(value);
    this.selectedCity = [];
    this.selection.city = [];
    this.city = undefined;
    this.setCity(value);
  }

  setCounty(value) {
    this.authService.getCounty(value)
      .subscribe(data => {
        this.county = data.data;
        this.countyLoading = false;
        $('[formControlName="county"]').removeClass('selectLoading');
      })
  }
  setCity(value) {
    this.authService.getCity(value).subscribe(data => {
      this.city = data.data;
      this.cityLoading = false;
      $('[formControlName="city"]').removeClass('selectLoading');
      $('#cover-spin').hide(0);
    })
  }
  setCityByCounty(value) {
    this.authService.getCityByCounty(this.selection.state.val + ',' + value).subscribe(data => {
      this.city = data.data;
      this.cityLoading = false;
      $('[formControlName="city"]').removeClass('selectLoading');
    },error=>{
      $('[formControlName="city"]').removeClass('selectLoading');
      this.cityLoading = false;
    })
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  changeCounty(arg) {
    //console.log(arg)
    this.cityLoading = true;
    $('[formControlName="city"]').addClass('selectLoading');
    const county = [];
    let arr: any = [];
    for (let i: number = 0; i < arg.length; i++) {
      county.push({ val: arg[i].split(/_(.+)/)[0], text: arg[i].split(/_(.+)/)[1] });
      arr.push(arg[i].split(/_(.+)/)[1]);
    }
    this.selection.county = county;
    this.selectedCity = [];
    this.selection.city = [];
    this.city = undefined;
    this.setCityByCounty(arr.toString());
  }
  changeCity(arg) {
    //console.log(arg)
    const city = [];
    for (let i: number = 0; i < arg.length; i++) {
      city.push({ val: arg[i].split(/_(.+)/)[0], text: arg[i].split(/_(.+)/)[1] });
    }
    this.selection.city = city;
  }
  changeZip(arg) {
    this.zipError = false;
    var num = arg.replace(/\D/g, "");
    if (num.length == 9) {
      arg = num.substr(0, 5) + '-' + num.substr(5);
      this.searchForm.patchValue({ 'zipcode': arg });
    }
    // if(num.length==10){
    //   arg = num.substr(0,5)+','+num.substr(5);
    //   this.searchForm.patchValue({ 'zipcode': arg});
    // }
    // if(num.length>10){
    //   arg = num.substr(0,5)+'-'+num.substr(5,4)+','+num.substr(10);
    //   this.searchForm.patchValue({ 'zipcode': arg});
    // }
    this.selection.zipcode = arg;
  }
  changeLand(arg) {
    const land = [];
    for (let i: number = 0; i < arg.length; i++) {
      land.push({ val: arg[i].split(/_(.+)/)[0], text: arg[i].split(/_(.+)/)[1] });
    }
    this.selection.land = land;
  }
  changeExemption(arg) {
    const exemption = [];
    for (let i: number = 0; i < arg.length; i++) {
      exemption.push({ val: arg[i].split(/_(.+)/)[0], text: arg[i].split(/_(.+)/)[1] });
    }
    this.selection.exemption = exemption;

  }
  changeOccupancy(arg) {

    const occupancy = [];
    for (let i: number = 0; i < arg.length; i++) {
      occupancy.push({ val: arg[i].split(/_(.+)/)[0], text: arg[i].split(/_(.+)/)[1] });
    }
    this.selection.occupancy = occupancy;
  }

  changeMortgageType(arg) {

    const mortgageType = [];
    for (let i: number = 0; i < arg.length; i++) {
      mortgageType.push({ val: arg[i].split(/_(.+)/)[0], text: arg[i].split(/_(.+)/)[1] });
    }
    this.selection.mortgageType = mortgageType;
  }

  changeMortgageMax(arg) {

    const mortgageMax = [];
    for (let i: number = 0; i < arg.length; i++) {
      mortgageMax.push({ val: arg[i].split(/_(.+)/)[0], text: arg[i].split(/_(.+)/)[1] });
    }
    this.selection.maxOpenLien = mortgageMax;
  }

  changeListingStatus(arg) {
    const listingStatus = [];
    for (let i: number = 0; i < arg.length; i++) {
      listingStatus.push({ val: arg[i].split(/_(.+)/)[0], text: arg[i].split(/_(.+)/)[1] });
    }
    this.selection.listingStatus = listingStatus;
  }
  selectToday() {
    this.model = this.calendar.getToday();
  }

  changeCountySelect(arg) {

    this.selection.countySelect = arg
  }

  changeCitySelect(arg) {

    this.selection.citySelect = arg
  }
  changeZipSelect(arg) {

    this.selection.zipSelect = arg
  }
  changeSale(arg) {
    this.selection.saleSelect = arg
  }

  saleFromDate(arg) {

    this.selection.salesFrom = arg
    if (this.selection.saleSelect != 'is') {
      if (!this.validateDate(arg, this.selection.salesTo)) {
        this.countError = true;
        this.saleDateError = true;
        return;
      }
    }
    this.countError = false;
    this.saleDateError = false;
  }
  saleToDate(arg) {
    this.selection.salesTo = arg
    if (this.selection.saleSelect != 'is') {
      if (!this.validateDate(this.selection.salesFrom, arg)) {
        this.countError = true;
        this.saleDateError = true;
        return;
      }
    }
    this.countError = false;
    this.saleDateError = false;
  }
  changeMortageStatus(arg) {
    this.selection.mortgageAmountSelect = arg
  }

  changeMortgageFrom(arg) {
    this.selection.mortgageAmountFrom = arg

    if (this.selection.mortgageAmountSelect != 'is') {
      if (!this.validateAmount(this.selection.mortgageAmountFrom, this.selection.mortgageAmountTo)) {
        this.countError = true;
        this.openAmountError = true;
        return;
      }
    }
    this.countError = false;
    this.openAmountError = false;

  }
  changeMortgageTo(arg) {
    this.selection.mortgageAmountTo = arg
    if (this.selection.mortgageAmountSelect != 'is') {
      if (!this.validateAmount(this.selection.mortgageAmountFrom, this.selection.mortgageAmountTo)) {
        this.countError = true;
        this.openAmountError = true;
        return;
      }
    }
    this.countError = false;
    this.openAmountError = false;
  }
  changeMortageDate(arg) {
    this.selection.mortgageRecordingDate = arg
  }

  mortgageDateFrom(arg) {
    this.selection.mortgageRecordingFrom = arg
    if (this.selection.mortgageRecordingDate != 'is') {
      if (!this.validateDate(arg, this.selection.mortgageRecordingTo)) {
        this.countError = true;
        this.openDateError = true;
        return;
      }
    }
    this.countError = false;
    this.openDateError = false;
  }

  mortgageDateTo(arg) {
    this.selection.mortgageRecordingTo = arg
    if (this.selection.mortgageRecordingDate != 'is') {
      if (!this.validateDate(this.selection.mortgageRecordingFrom, arg)) {
        this.countError = true;
        this.openDateError = true;
        return;
      }
    }
    this.countError = false;
    this.openDateError = false;
  }

  mortgageInterest(arg) {
    this.selection.mortgageInterestStatus = arg
  }

  changeMortgageInterestFrom(arg) {
    this.selection.mortgageInterestFrom = arg

    if (this.selection.mortgageInterestStatus != 'is') {
      if (!this.validateAmount(this.selection.mortgageInterestFrom, this.selection.mortgageInterestTo)) {
        this.countError = true;
        this.openInterestError = true;
        return;
      }
    }
    this.countError = false;
    this.openInterestError = false;


  }
  changeMortgageInterestTo(arg) {
    this.selection.mortgageInterestTo = arg
    if (this.selection.mortgageInterestStatus != 'is') {
      if (!this.validateAmount(this.selection.mortgageInterestFrom, this.selection.mortgageInterestTo)) {
        this.countError = true;
        this.openInterestError = true;
        return;
      }
    }
    this.countError = false;
    this.openInterestError = false;
  }

  equityChange(arg) {
    this.selection.equityStatus = arg
  }
  equityChangeFrom(arg) {
    this.selection.equityFrom = arg
    if (this.selection.equityStatus != 'is') {
      if (!this.validateAmount(this.selection.equityFrom, this.selection.equityTo)) {
        this.countError = true;
        this.equityError = true;
        return;
      }
    }
    this.countError = false;
    this.equityError = false;

  }
  equityChangeTo(arg) {
    this.selection.equityTo = arg
    if (this.selection.equityStatus != 'is') {
      if (!this.validateAmount(this.selection.equityFrom, this.selection.equityTo)) {
        this.countError = true;
        this.equityError = true;
        return;
      }
    }
    this.countError = false;
    this.equityError = false;
  }

  listingPriceChange(arg) {
    this.selection.listingPriceStatus = arg
  }

  listingPriceChangeFrom(arg) {
    this.selection.listingPriceFrom = arg
    if (this.selection.listingPriceStatus != 'is') {
      if (!this.validateAmount(this.selection.listingPriceFrom, this.selection.listingPriceTo)) {
        this.countError = true;
        this.listingError = true;
        return;
      }
    }
    this.countError = false;
    this.listingError = false;
  }
  listingPriceChangeTo(arg) {
    this.selection.listingPriceTo = arg
    if (this.selection.listingPriceStatus != 'is') {
      if (!this.validateAmount(this.selection.listingPriceFrom, this.selection.listingPriceTo)) {
        this.countError = true;
        this.listingError = true;
        return;
      }
    }
    this.countError = false;
    this.listingError = false;
  }

  forclosureStatusChange(arg) {
    var indexSel = arg.target.selectedIndex;
    if (indexSel == 0) {
      this.selection.forclosureStatus = null
      this.searchForm.patchValue({
        forclosureDateFrom: [''],
        forclosureDateTo: [''],
        forclosureAmountFrom: [''],
        forclosureAmountTo: ['']
      })
      this.selection.forclosureDateFrom = ''
      this.selection.forclosureDateTo = ''
      this.selection.forclosureAmountFrom = ''
      this.selection.forclosureAmountTo = ''
      this.searchForm.controls['forclosureDateFrom'].disable()
      this.searchForm.controls['forclosureDateTo'].disable()
      this.searchForm.controls['forclosureAmountFrom'].disable()
      this.searchForm.controls['forclosureAmountTo'].disable()
    } else {
      var label = arg.srcElement[indexSel].label;
      var value = arg.srcElement[indexSel].value;
      this.selection.forclosureStatus = { val: value, text: label }
      this.searchForm.controls['forclosureDateFrom'].enable()
      this.searchForm.controls['forclosureDateTo'].enable()
      this.searchForm.controls['forclosureAmountFrom'].enable()
      this.searchForm.controls['forclosureAmountTo'].enable()
    }
  }

  forclosureDateChange(arg) {
    this.selection.forclosureDateStatus = arg
  }

  forclosureDateChangeFrom(arg) {
    this.selection.forclosureDateFrom = arg
    if (this.selection.forclosureDateStatus != 'is') {
      if (!this.validateDate(arg, this.selection.forclosureDateTo)) {
        this.countError = true;
        this.forDateError = true;
        return;
      }
    }
    this.countError = false;
    this.forDateError = false;


  }
  forclosureDateChangeTo(arg) {
    this.selection.forclosureDateTo = arg

    if (this.selection.forclosureDateStatus != 'is') {
      if (!this.validateDate(this.selection.forclosureDateFrom, arg)) {
        this.countError = true;
        this.forDateError = true;
        return;
      }
    }
    this.countError = false;
    this.forDateError = false;
  }
  forclosureAmountChange(arg) {
    this.selection.forclosureAmountStatus = arg
  }

  forclosureAmountChangeFrom(arg) {
    this.selection.forclosureAmountFrom = arg
    if (this.selection.forclosureAmountStatus != 'is') {
      if (!this.validateAmount(this.selection.forclosureAmountFrom, this.selection.forclosureAmountTo)) {
        this.countError = true;
        this.forAmountError = true;
        return;
      }
    }
    this.countError = false;
    this.forAmountError = false;
  }

  forclosureAmountChangeTo(arg) {
    this.selection.forclosureAmountTo = arg
    if (this.selection.forclosureAmountStatus != 'is') {
      if (!this.validateAmount(this.selection.forclosureAmountFrom, this.selection.forclosureAmountTo)) {
        this.countError = true;
        this.forAmountError = true;
        return;
      }
    }
    this.countError = false;
    this.forAmountError = false;
  }

  forDis() {
    if (this.selection.forclosureStatus == null) {
      return true;
    }
    else {
      return false;
    }
  }





  ngOnInit() {
    Inputmask({ autoUnmask: true }).mask(document.querySelectorAll("input"));
    if (this.advanceSearchFilledForm) {
      $('#cover-spin').show(0);
      this.selection = this.advanceSearchFilledForm;
      this.searchForm.patchValue({
        state: this.advanceSearchFilledForm.state.val,
        countySelect: this.advanceSearchFilledForm.countySelect,
        // county:[''],
        //city:[''],
        citySelect: this.advanceSearchFilledForm.citySelect,
        zipSelect: this.advanceSearchFilledForm.zipSelect,
        zipcode: this.advanceSearchFilledForm.zipcode,
        // land:[''],
        //  landRValue:[''],
        //  landCValue:[''],
        // exemption:[''],
        // occupancy:[''],
        saleSelect: this.advanceSearchFilledForm.saleSelect,
        salesFrom: this.convertDatefromat(this.advanceSearchFilledForm.salesFrom),
        salesTo: this.convertDatefromat(this.advanceSearchFilledForm.salesTo),
        mortgageAmountSelect: this.advanceSearchFilledForm.mortgageAmountSelect,
        mortgageAmountFrom: this.advanceSearchFilledForm.mortgageAmountFrom,
        mortgageAmountTo: this.advanceSearchFilledForm.mortgageAmountTo,
        mortgageRecordingDate: this.advanceSearchFilledForm.mortgageRecordingDate,
        mortgageRecordingFrom: this.convertDatefromat(this.advanceSearchFilledForm.mortgageRecordingFrom),
        mortgageRecordingTo: this.convertDatefromat(this.advanceSearchFilledForm.mortgageRecordingTo),
        //  mortgageType:[''], 
        mortgageInterestStatus: this.advanceSearchFilledForm.mortgageInterestStatus,
        mortgageInterestFrom: this.advanceSearchFilledForm.mortgageInterestFrom,
        mortgageInterestTo: this.advanceSearchFilledForm.mortgageInterestTo,
        //   maxOpenLien:[''],
        equityStatus: this.advanceSearchFilledForm.equityStatus,
        equityFrom: this.advanceSearchFilledForm.equityFrom,
        equityTo: this.advanceSearchFilledForm.equityTo,
        //  listingStatus:[''],
        listingPriceStatus: this.advanceSearchFilledForm.listingPriceStatus,
        listingPriceFrom: this.advanceSearchFilledForm.listingPriceFrom,
        listingPriceTo: this.advanceSearchFilledForm.listingPriceTo,
        forclosureStatus: this.advanceSearchFilledForm.forclosureStatus,
        forclosureDateStatus: this.advanceSearchFilledForm.forclosureDateStatus,
        forclosureDateFrom: this.convertDatefromat(this.advanceSearchFilledForm.forclosureDateFrom),
        forclosureDateTo: this.convertDatefromat(this.advanceSearchFilledForm.forclosureDateTo),
        forclosureAmountStatus: this.advanceSearchFilledForm.forclosureAmountStatus,
        forclosureAmountFrom: this.advanceSearchFilledForm.forclosureAmountFrom,
        forclosureAmountTo: this.advanceSearchFilledForm.forclosureAmountTo,
        // savedTitle:this.advanceSearchFilledForm.savedTitle,
        // purchaseGroupName:this.advanceSearchFilledForm.purchaseGroupName
      });
      if (this.advanceSearchFilledForm.state) {
        this.setCounty(this.advanceSearchFilledForm.state.val);
        this.setCity(this.advanceSearchFilledForm.state.val);
      }
      if (this.advanceSearchFilledForm.county) {
        this.selectedCounty = [];
        let tmpCountyArr: any = [];
        this.advanceSearchFilledForm.county.forEach(arr => {
          this.selectedCounty.push(arr.val + '_' + arr.text);
          tmpCountyArr.push(arr.text);
        });
        this.setCityByCounty(tmpCountyArr.toString());
      }
      if (this.advanceSearchFilledForm.city) {
        this.selectedCity = [];
        this.advanceSearchFilledForm.city.forEach(arr => { this.selectedCity.push(arr.val + '_' + arr.text); });
      }
      if (this.advanceSearchFilledForm.land) {
        this.selectedLand = [];
        this.advanceSearchFilledForm.land.forEach(arr => { this.selectedLand.push(arr.val + '_' + arr.text); });
      }
      if (this.advanceSearchFilledForm.exemption) {
        this.selectedExemption = [];
        this.advanceSearchFilledForm.exemption.forEach(arr => { this.selectedExemption.push(arr.val + '_' + arr.text); });
      }
      if (this.advanceSearchFilledForm.occupancy) {
        this.selectedOccupancy = [];
        this.advanceSearchFilledForm.occupancy.forEach(arr => { this.selectedOccupancy.push(arr.val + '_' + arr.text); });
      }
      if (this.advanceSearchFilledForm.mortgageType) {
        this.selectedMortgageType = [];
        this.advanceSearchFilledForm.mortgageType.forEach(arr => { this.selectedMortgageType.push(arr.val + '_' + arr.text); });
      }
      if (this.advanceSearchFilledForm.maxOpenLien) {
        this.selectedMaxOpenLien = [];
        this.advanceSearchFilledForm.maxOpenLien.forEach(arr => { this.selectedMaxOpenLien.push(arr.val + '_' + arr.text); });
      }
      if (this.advanceSearchFilledForm.listingStatus) {
        this.selectedListingStatus = [];
        this.advanceSearchFilledForm.listingStatus.forEach(arr => { this.selectedListingStatus.push(arr.val + '_' + arr.text); });
      }
      if (!this.advanceSearchFilledForm.city) {
        $('#cover-spin').hide(0);
      }
      //$('#cover-spin').hide(0);
    }
    //$('.money').mask("#,##0.00", {reverse: true});
    // $("#zipcode").keypress(function(e){
    //   var keyCode = e.which; 
    //   if ( !(keyCode >= 48 && keyCode <= 57) && keyCode != 8 && keyCode != 32 && keyCode != 45) {// && keyCode != 44
    //     e.preventDefault();
    //   }
    // });

    this.depositeForm = this.fb.group({
      amount: [{value: ''}, [Validators.required]],
    })
    const that = this;
    // this.adSrData.currentSearchData.subscribe(data => this.data = data);    
    // this.authService.getProfile().subscribe((data)=>{           
    //   if(data.data.member){
    //     this.existingMember = true;
    //   } else{
    //     this.existingMember = false;
    //     this.authService.getBecomeMemberPopupCntnt().subscribe((data)=>{        
    //       console.log(data)
    //       this.modalTitle = data.become_member_popup_title;
    //       this.modalContent = data.become_member_popup_content;
    //     },(error)=>{
    //       console.log(error)
    //     })
    //   }

    //   console.log(data)
    // },(error)=>{
    //   console.log(error)
    // });

    if (localStorage.getItem('paidMember')) {
      this.existingMember = true;
    } else {
      this.existingMember = false;
      this.authService.getBecomeMemberPopupCntnt().subscribe((data) => {
        console.log(data)
        this.modalTitle = data.become_member_popup_title;
        this.modalContent = data.become_member_popup_content;
      }, (error) => {
        console.log(error)
      })
    }
    $('#sss').change(function () {
      console.log('ss')
    });

    // $('#multipleSelectExample').select2().on('change', function (e) {
    //     const county=[];
    //     //alert($(this).val())
    //     $('#multipleSelectExample option:selected').map(function (index,item ) { 
    //     const val=$(this).val()
    //     var n = val.match(/'(.*?)'/g).toString();
    //     var m = n.replace(/'/g,"");
    //      county.push({val:m,text:$(this).text()})
    //     })
    //     that.selection.county=county;
    //     let arr:any =[];
    //     for(let i:number=0; i<county.length; i++) {            
    //         arr.push(county[i].text);
    //     }
    //    // this.setCity(arr.toString());              
    // });
    interface AfterViewInit {
      ngAfterViewInit(): void

    }

    // $('#multipleSelectExample3').select2().on('change', function (e) {
    //   const city=[];
    //   $('#multipleSelectExample3 option:selected').map(function (index,item ) { 
    //   const val=$(this).val()
    //   var n = val.match(/'(.*?)'/g).toString();
    //   var m = n.replace(/'/g,"");
    //    city.push({val:m,text:$(this).text()})
    //   })
    //   that.selection.city=city;
    // });

    // $('#multipleSelectExample2').select2().on('change', function () {
    //   const land=[];
    //   $('#multipleSelectExample2 option:selected').each(function (index,item ) { 
    //     const val=$(this).val()
    //     var n = val.match(/'(.*?)'/g).toString();
    //     var m = n.replace(/'/g,"");
    //     land.push({val:m,text:$(this).text()})
    //   })
    //   that.selection.land=land;

    // })

    // $('#multipleSelectExample4').select2().on('change', function () {
    //   const exemption=[];
    //   $('#multipleSelectExample4 option:selected').each(function (index,item ) { 
    //     const val=$(this).val()
    //     var n = val.match(/'(.*?)'/g).toString();
    //     var m = n.replace(/'/g,"");
    //     exemption.push({val:m,text:$(this).text()})
    //   })
    //   that.selection.exemption=exemption;

    // })

    // $('#multipleSelectExample5').select2().on('change', function () {
    //   const occupancy=[];
    //   $('#multipleSelectExample5 option:selected').each(function (index,item ) { 
    //     const val=$(this).val()
    //     var n = val.match(/'(.*?)'/g).toString();
    //     var m = n.replace(/'/g,"");
    //     occupancy.push({val:m,text:$(this).text()})
    //   })
    //   that.selection.occupancy=occupancy;

    // })

    // $('#multipleSelectExample14').select2().on('change', function () {
    //   const type=[];
    //   $('#multipleSelectExample14 option:selected').each(function (index,item ) {
    //     const val=$(this).val()
    //     var n = val.match(/'(.*?)'/g).toString();
    //     var m = n.replace(/'/g,"");
    //     type.push({val:m,text:$(this).text()})
    //   })
    //   that.selection.mortgageType=type;

    // })


    // $('#multipleSelectExample15').select2().on('change', function () {
    //   const lien=[];
    //   $('#multipleSelectExample15 option:selected').each(function (index,item ) { 
    //     const val=$(this).val()
    //     var n = val.match(/'(.*?)'/g).toString();
    //     var m = n.replace(/'/g,"");
    //     lien.push({val:m,text:$(this).text()})
    //   })
    //   that.selection.maxOpenLien=lien;

    // })
    // $('#multipleSelectExample16').select2().on('change', function () {
    //   const listing=[];
    //   $('#multipleSelectExample16 option:selected').each(function (index,item ) { 
    //     const val=$(this).val()
    //     var n = val.match(/'(.*?)'/g).toString();
    //     var m = n.replace(/'/g,"");
    //     listing.push({val:m,text:$(this).text()})
    //   })
    //   that.selection.listingStatus=listing;

    // })




    this.authService.getSearchPage()
      .subscribe((data) => {
        this.kicks = data.data.kicks
      })

  }
  submitSearchForm() {
    if (!this.saveSearchForm.valid) {
      console.log('error')
      return;
    }
    this.savedsearchFlag = true;
  }

  getMonthName(mon) {
    return ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][mon - 1];
  }
  submitForm() {
    if (this.searchForm.value["state"] == null) {
      this.toastr.error('State filed is mandatory.', 'Error!');
      return;
    }
    this.savedsearchFlag = false;
    if (this.searchForm.controls.zipcode.errors) {
      this.zipError = true;
      this.toastr.error('Please Remove Error First!', 'Error!');
      return;
    }
    if (this.countError) {
      //alert('Please Remove error First');
      this.toastr.error('Please Remove Error First!', 'Error!');
      return
    }
    this.submit = true;
    this.authService.getCount(this.selection).subscribe(data => {
      this.SearchCount.count = data.data.MaxResultsCount
      //////
      // let tmp:any =this.authService.getWalletPintsToDeduct(this.SearchCount.count);
      // this.SearchCount.perPropertyRate = parseInt(tmp);
      // console.log(this.selection);
      ////////
      this.submit = false;
      this.myForm = this.selection;
    }, error => {
      console.log(error)
      this.toastr.error(error, 'Error!');
      this.submit = false;
    })
    //console.log(this.selection)
    // this.authService.postSearch(this.searchForm.value)
    // .subscribe((data)=>{
    //   alert(data.message);
    //   this.searchForm.reset()
    // },(error)=>{
    //   alert(error)
    // })
    // this.submit=false
  }

  onModal(arg) {
    
    this.depositeForm.patchValue({
      "amount": arg<50? 50 :arg
    });
    this.depositeForm.get('amount').setValidators([Validators.min(this.minAmount())]);

    if(this.saveSearchForm.value && this.saveSearchForm.value.purchaseGroupName) {

      this.authService.checkPurchaseGroupNameDuplicate({ purchase_group_name: this.saveSearchForm.value.purchaseGroupName})
      .subscribe(
        (data: any) => {
          $('#myModal').modal('show');
        },
        (err: any) => {
          this.toastr.error(err, 'Error!');
        }
      )

    } else {
      $('#myModal').modal('show');
    }

  }

  deposite() {
    $('#cover-spin').show(0);
    this.submitWallet = true;
    if (!this.depositeForm.valid) {
      console.log('error')
      $('#cover-spin').hide(0);
      return;
    }
    this.authService.walletRecharge(this.depositeForm.value)
      .subscribe((data) => {
        console.log(data)
        //alert(data.message);
        $('#cover-spin').hide(0);
        this.toastr.success('Wallet Recharged Successfully!', 'Success!');
        //alert('')
        this.getDeductionToPur();
        this.depositeForm.reset();
        this.submitWallet = false;
        $('#myModal').modal('hide');
        $('#myConfirmModal').modal('hide');

      }, (error) => {
        console.log(error)
        $('#myModal').modal('hide');
        $('#cover-spin').hide(0);
        this.toastr.error(error, 'Error!');
        //alert(error)
      })
  }
  getResult(e) {
    // console.log('test')
    $('#cover-spin').show(0);
    this.confirmBoxContent = '';
    this.saveSearchForm.reset();
    if (this.existingMember) {
      localStorage.removeItem('advanceSearchForm');
      this.submit = true;
      this.authService.getWallet().subscribe(data => {
        this.currentBalance = parseFloat(data.data.current_points);
        this.SearchCount.perPropertyRate = data.data.perPropertyRate;
        console.log(this.SearchCount.amountReq())
        console.log(this.currentBalance.toFixed(2))
        if (parseFloat(this.currentBalance.toFixed(2)) >= this.SearchCount.amountReq()) {
          this.reqAmountInWalletToPur = false;
          this.confirmBoxContent = this.SearchCount.amountReq() + " will be deduct from " + this.currentBalance + " in Wallet."
          this.submit = false;
          $('#cover-spin').hide(0);
          console.log('if');
        } else {
          this.reqAmountInWalletToPur = true;
          this.depositeForm.patchValue({
            'currentBalance': this.currentBalance,
            'perPropertyRate': this.SearchCount.perPropertyRate,
            'recordCount': this.SearchCount.count
          });
          this.confirmBoxContent = "You don't have sufficient balance. Please recharge your wallet. " + this.SearchCount.amountReq() + " will be required to purchase these records. "
          this.confirmBoxReqAmt = this.SearchCount.amountReq();
          if (this.currentBalance > 0) {
            this.confirmBoxContent += "Currently you have " + this.currentBalance + " in Wallet. "
            this.confirmBoxReqAmt = this.confirmBoxReqAmt - this.currentBalance;
          }
          this.confirmBoxContent += this.SearchCount.amountReq() + " will be automacically debited once you recharge Wallet."
          this.submit = false;
          $('#cover-spin').hide(0);
        }
      }, error => {
        this.submit = false;
        console.log(error)
        $('#cover-spin').hide(0);
        this.toastr.error(error, 'Error!');
        this.confirmBoxContent += "No deduction occurred as we are unable to fetch records at the moment. Try again after sometime."
        //alert(error)
      });
    } else {
      this.load=0;
      localStorage.setItem('advanceSearchForm', JSON.stringify(this.selection));
      // this.confirmBoxContent = this.modalContent;
      $('#cover-spin').hide(0);  
      e.stopPropagation();
    }
  }

  getDeductionToPur() {
    if (!this.saveSearchForm.valid) {
      return;
    }

    if(this.saveSearchForm.value && this.saveSearchForm.value.purchaseGroupName) {

      this.authService.checkPurchaseGroupNameDuplicate({ purchase_group_name: this.saveSearchForm.value.purchaseGroupName})
      .subscribe(
        (data: any) => {
          this.getDeductionToPur2();
        },
        (err: any) => {
          this.toastr.error(err, 'Error!');
        }
      )

    } else {
      this.getDeductionToPur2();
    }
    
  }

  getDeductionToPur2() {
    $('#myConfirmModal').modal('hide');
    $('#cover-spin').show(0);
    this.selection.savedTitle = this.saveSearchForm.value.savedTitle;
    this.selection.purchaseGroupName = this.saveSearchForm.value.purchaseGroupName;

    $('#cover-spin').hide(0);
    this.modalService.open(this.content2, { size: 'md' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      $('#cover-spin').show(0);
      this.authService.saveSearch(this.myForm).subscribe(data => {
        if (!this.reqAmountInWalletToPur) {//|| this.currentBalance > 0
          const reqAmt = (this.SearchCount.amountReq() > this.currentBalance) ? this.currentBalance : this.SearchCount.amountReq();
          // this.authService.updateWallet(reqAmt).subscribe(data => {
          //   this.toastr.success('Records Purchased Successfully!', 'Success!');
  
          // }, error => {
          //   console.log(error)
          //   this.toastr.error(error, 'Error!');
          //   //alert(error)
          // })
  
          this.authService.getResult(this.myForm).subscribe(data => {
            this.router.navigate(['/customer/purchased-list/' + encodeURIComponent(data.data.purchase_group_name)]);
            $('#cover-spin').hide(0);
          }, error => {
            $('#cover-spin').hide(0);
            console.log(error)
            this.toastr.error(error, 'Error!');
            //alert(error)
          })
  
  
        } else {
          this.toastr.success('Records Purchased Successfully!', 'Success!');
          this.authService.getResult(this.myForm).subscribe(data => {
            this.router.navigate(['/customer/purchased-list/' + encodeURIComponent(data.data.purchase_group_name)]);
            $('#cover-spin').hide(0);
          }, error => {
            $('#cover-spin').hide(0);
            console.log(error)
            this.toastr.error(error, 'Error!');
            //alert(error)
          })
        }
      }, error => {
        $('#cover-spin').hide(0);
        console.log(error)
        this.toastr.error(error, 'Error!');
        //alert(error)
      }) 

    }, (reason) => {
      this.closeResult = `Dismissed`;
    });
  }


  validateDate(fDate, tDate) {
    if (fDate && tDate) {
      let fromDate = new Date(fDate.year, fDate.month - 1, fDate.day);
      let toDate = new Date(tDate.year, tDate.month - 1, tDate.day);
      if (fromDate > toDate) {
        return false;
      }
    }
    if (!fDate && tDate || fDate && !tDate) {
      return false;
    }
    return true;
  }
  validateAmount(fAmount, tAmount) {
    if (fAmount && tAmount) {
      if (parseInt(fAmount) > parseInt(tAmount)) {
        return false;
      }
    }
    if (!fAmount && tAmount || fAmount && !tAmount) {
      return false;
    }
    return true;
  }
  remove(key: string, item: any, type: string, key2: string) {
    //alert(item);
    if (key == 'state') {
      this.selection.state = null;
      this.selectedCounty = [];
      this.selection.county = [];
      this.county = undefined;
      this.selectedCity = [];
      this.selection.city = [];
      this.city = undefined;
      this.searchForm.patchValue({ 'state': null });
    }
    else if (type == 'multySelect') {
      let selectedArr = 'selected' + key.charAt(0).toUpperCase() + key.slice(1);
      const index: number = this[selectedArr].indexOf(item.val + '_' + item.text);
      $('[formcontrolname="' + key + '"] .ng-value-container .ng-value:eq(' + index + ') .ng-value-icon.left').click();
    }
    else if (type == 'txtInput') {
      this.searchForm.get(key).setValue('');
      this.selection[key] = '';
    }
    else if (type == 'txtInput2') {
      this.searchForm.get(key).setValue('');
      this.searchForm.get(key2).setValue('');
      this.selection[key] = '';
      this.selection[key2] = '';
    }
    else if (type == 'dateInput') {
      this.searchForm.get(key).setValue('');
    }
    else if (type == 'dateInput2') {
      this.searchForm.get(key).setValue('');
      this.searchForm.get(key2).setValue('');
    }
    else if (key == 'forclosureStatus') {
      this.searchForm.patchValue({ 'forclosureStatus': null });
      this.selection.forclosureStatus = null
      this.searchForm.patchValue({
        forclosureDateFrom: [''],
        forclosureDateTo: [''],
        forclosureAmountFrom: [''],
        forclosureAmountTo: ['']
      })
      this.selection.forclosureDateFrom = ''
      this.selection.forclosureDateTo = ''
      this.selection.forclosureAmountFrom = ''
      this.selection.forclosureAmountTo = ''
      this.searchForm.controls['forclosureDateFrom'].disable()
      this.searchForm.controls['forclosureDateTo'].disable()
      this.searchForm.controls['forclosureAmountFrom'].disable()
      this.searchForm.controls['forclosureAmountTo'].disable()
    }
  }
  convertDatefromat(data) {
    if (data) {
      data.year = parseInt(data.year)
      data.month = parseInt(data.month)
      data.day = parseInt(data.day)
    }
    return data;
  }


  minAmount() {

    if((this.SearchCount.amountReq() - this.currentBalance) >= 50) 
      return this.SearchCount.amountReq() - this.currentBalance;
    else return 50;
  }


}